package com.dhcc.tke;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TkeApplication {

	public static void main(String[] args) {
		SpringApplication.run(TkeApplication.class, args);
	}

}
