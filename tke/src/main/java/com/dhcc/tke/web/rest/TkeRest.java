package com.dhcc.tke.web.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TkeRest {
	
	private static Log logger = LogFactory.getLog(TkeRest.class);
	
	@RequestMapping(value = "/echo/{param}", method = RequestMethod.GET)
    public String echo(@PathVariable String param) {
		logger.info("tke -- request param: [" + param + "]");
        String result = "request param: " + param + ", response from tke";
        return result;
    }
}
